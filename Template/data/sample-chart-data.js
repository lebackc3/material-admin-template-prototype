var sampleChartData = {
    "chart_title" : "Bar-1",
    "data_sets": [{
        "label" : "Trend",
        "data" : [[1, 50], [2, 30], [3, 70], [4, 50], [5, 30], [6, 70], [7, 50], [8, 30], [9, 70], [10, 50], [11, 30], [12, 70]],
        "color" : "#edeff0"
    }]
}
