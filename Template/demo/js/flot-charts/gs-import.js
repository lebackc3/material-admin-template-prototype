console.clear()

const dataUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vReNj_cdOEjllD0kIF9KfNGH0cohNbfxt4jYH8HXH8opGkAkvdFXKCvbNf3SFCh4SzyZGBU_wgc_ANA/pubhtml'
//const metaUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQObuWCe-RHHn3qJgLB3dCGG1IgtisQadHDMvYkF4_7iCcQ-vQDqpRL9m3At3UEPteNYrAeQ9pCAU2m/pub?output=csv'

const request = async (url, query) => {
  const response = await fetch(url, {mode: 'cors'});
  const csv = await response.text();
  //$(query).html(csv.replace('\n', '<br/>'))
  console.log(csv.replace('\n', '<br/>'));
}

$(document).ready(() => {
  //const meta = request(metaUrl, '.meta')
  const data = request(dataUrl, '.data')
})