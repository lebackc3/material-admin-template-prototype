'use strict';

$(document).ready(function(){

    var charts = ['pieChart1', 'pieChart2', 'pieChart3', 'pieChart4', 'pieChart5', 'pieChart6']
    const colors = (data) => {
        let colors = [];
        data.forEach(element => {
            colors.push((element > 20) ? 'green' : 'red');
        });
        return colors; 
    }

    const newPieData = (length, max, min) => {
        var data = [];
        for(let i = 0; i < length; i++) {
            data.push(Math.floor(Math.random() * (max - min + 1) ) + min);
            data.push(0);
        }
        return data;
    }

    charts.forEach(element => {
        var data = {
            datasets: [{
                data: newPieData(8, 50, 0),
                backgroundColor: 'rgb(28, 137, 232)',
                borderColor: 'rgba(180, 191, 195, 0)',
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
    
        };
        
        var ctx = document.getElementById(element).getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'polarArea',
            data: data,            
        
            // Configuration options go here
            options: {}
        });
    });
    

});