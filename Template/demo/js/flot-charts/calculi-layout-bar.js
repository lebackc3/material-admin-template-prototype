'use strict';

$(document).ready(function(){

    const newData = (length, max, min) => {
        var data = [];
        for(let i = 0; i < length; i++) {
            var data_set = [];
            data_set.push(i + 1);
            data_set.push(Math.floor(Math.random() * (max - min + 1) ) + min);
            data.push(data_set);
        }
        return data;
    }

    const colors = (data) => {
        let colors = [];
        data.forEach(element => {
            colors.push((element > 20) ? 'green' : 'red');
        });
        return colors; 
    }

    // Data Set 1
    var graph1_data1 = newData(19, 100, 0);
    var graph1_data2 = newData(19, 100, 0);
    var graph1_data3 = newData(19, 100, 0);

    // Bar Data 1
    var barData1 = [
        {
            label: 'Tokyo',
            data: graph1_data1,
            color: '#edeff0',
            bars: {
                order: 0
            }
        },
        {
            label: 'Seoul',
            data: graph1_data2,
            color: '#8a99a0',
            bars: {
                order: 1
            }
        },
        {
            label: 'Beijing',
            data: graph1_data3,
            color: '#415158',
            bars: {
                order: 2
            }
        }
    ]

    // Data Set 2
    var graph2_data1 = newData(19, 100, -100);
    var graph2_data2 = newData(19, 100, -100);
    var graph2_data3 = newData(19, 100, -100);

    // Bar Data 2
    var barData2 = [
        {
            label: 'Tokyo',
            data: graph2_data1,
            color: '#edeff0',
            bars: {
                order: 0
            }
        },
        {
            label: 'Seoul',
            data: graph2_data2,
            color: '#8a99a0',
            bars: {
                order: 1
            }
        },
        {
            label: 'Beijing',
            data: graph2_data3,
            color: '#415158',
            bars: {
                order: 2
            }
        }
    ]

    // Let's create the chart
    if ($('#chart-bar-1')[0]) {
        $.plot($("#chart-bar-1"), barData1, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.05,
                    fill: 1
                }
            },
            grid : {
                    borderWidth: 1,
                    borderColor: '#31424b',
                    show : true,
                    hoverable : true
            },

            yaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            xaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            legend:{
                container: '.flot-chart-legend--bar1',
                noColumns: 5
            }
        });
    }
    if ($('#chart-bar-2')[0]) {
        $.plot($("#chart-bar-2"), barData2, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.05,
                    fill: 1
                }
            },
            grid : {
                    borderWidth: 1,
                    borderColor: '#31424b',
                    show : true,
                    hoverable : true
            },

            yaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            xaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            legend:{
                container: '.flot-chart-legend--bar2',
                noColumns: 5
            }
        });
    }

    // Bar Data 2
    var barData3 = sampleChartData.data_sets;

    // Let's create the chart
    if ($('#chart-bar-3')[0]) {
        $.plot($("#chart-bar-3"), barData3, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.05,
                    fill: 1
                }
            },
            grid : {
                    borderWidth: 1,
                    borderColor: '#31424b',
                    show : true,
                    hoverable : true
            },

            yaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            xaxis: {
                tickColor: '#31424b',
                tickDecimals: 0,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#98a7ac"
                },
                shadowSize: 0
            },

            legend:{
                container: '.flot-chart-legend--bar3',
                noColumns: 5
            }
        });
    }
   

    // Tooltips for Flot Charts
    if ($('.flot-chart')[0]) {
        $('.flot-chart').bind('plothover', function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                $('.flot-tooltip').html(item.series.label + ' of ' + x + ' = ' + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

        $("<div class='flot-tooltip'></div>").appendTo("body");
    }
});
