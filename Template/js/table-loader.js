
$(window).load(function() {
    let col_count = sampleData.data_categories.length;
    let row_count = sampleData.data.length;

    for (var i = 0; i < col_count; i++) {
      let $new_th = $("<th>" + sampleData.data_categories[i] + "</th>")
      $("#table-header").append($new_th);
    }

    for (var i = 0; i < row_count; i++) {
      let $new_row = $("<tr></tr>");
      for (var j = 0; j < col_count; j++) {
        let $new_td = $("<td>" + sampleData.data[i][j] + "</td>");
        $new_row.append($new_td);
      }
      $("#data-table").append($new_row);
    }
});

function handleFileSelect() {

  input = document.getElementById('data-source');
  if (!input) {
    alert("Um, couldn't find the fileinput element.");
  }
  else if (!input.files) {
    alert("This browser doesn't seem to support the `files` property of file inputs.");
  }
  else if (!input.files[0]) {
    alert("Please select a file before clicking 'Load'");               
  }
  else {
    let file = input.files[0];
    let fr = new FileReader();
    fr.onload = function(e) {
      var data = fr.result;
      console.log($.csv.toObjects(data));
    };

    fr.readAsText(file);
  }

}

$(".new-tab").click(function() {
    tab_count++;
    let $tab = $(`
      <div class = "tab selected" id = tab` +         tab_count + `>
        <div class = "close-tab">X</div>
      </div>
    `);
    
    let $page = $(`
      <div class = "page selected" id = page` + tab_count + `>   </div>  
    `);
    
    $(".tab, .page").removeClass("selected");
    $(this).before($tab);
    $(".content").append($page);
    
    $(".tab").click(function() {
      tab_index = $(this).index() + 1;
      $(".tab, .page").removeClass("selected");
      $(".header .tab:nth-child(" + tab_index + ")").addClass("selected");
      $(".content .page:nth-child(" + tab_index + ")").addClass("selected");
    });
    
    $(".close-tab").click(function() {
      tab_count--;
      tab_index = $(this).parent().index() + 1;
      console.log(tab_index);
      $(".header .tab:nth-child(" + tab_index + ")").remove();
      $(".content .page:nth-child(" + tab_index + ")").remove();   
    });
  });
  