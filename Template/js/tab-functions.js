let tab_index = 1;
let sample_name = "Sample Tab";

jQuery(".new-layout-button").click(function() {
  tab_index++;
  let tab_header_id = jQuery(this).attr('data-header-id');
  let $tab = jQuery(`
    <li id="li-tab-${tab_index}" class="tab active">
        <a class="col-xs-4" href="#tab-${tab_index}" data-toggle="tab" aria-expanded="true">
            ${sample_name}
            <div data-tab-id="#li-tab-${tab_index}" data-pane-id="#tab-${tab_index}" class = "close-tab-btn">X</div>
        </a>
    </li>
  `);

  let $page = jQuery(`
    <div class="tab-pane fade active in" id="tab-${tab_index}">
      <iframe id="iframe${tab_index}" width="100%" src="${jQuery(this).attr('data-layout')}.html" frameborder="0" scrolling="no"></iframe>
    </div>
  `);


  jQuery(tab_header_id).before($tab);
  jQuery(".tab-content").append($page);
  jQuery(".dropdown").removeClass("open");
  jQuery(".dropdown > a").attr('aria-expanded', 'false')
  jQuery(".dashboard-pane > .tab, .pipeline-pane > .tab").not(`#li-tab-${tab_index}`).removeClass("active");
  jQuery(".tab-content > .tab-pane").not(`#tab-${tab_index}`).removeClass("active in");

  iFrameResize({
    //log: true,
    heightCalculationMethod: 'min'
  }, `#iframe${tab_index}`);

  jQuery(".close-tab-btn").click(function() {
    let tab_id = jQuery(this).attr('data-tab-id');
    let pane_id = jQuery(this).attr('data-pane-id');
    jQuery(tab_id).remove();
    jQuery(pane_id).remove();
  });

  return false;
});

/*
jQuery(".new-tab-btn").click(function() {
  tab_index = jQuery(this).index() + 1;
  console.log(tab_index);
  let $tab = jQuery(`
    <li class="tab active">
        <a class="col-xs-4" href="#tab-${tab_index}" data-toggle="tab" aria-expanded="true">
            ${sample_name}
            <div class = "close-tab-btn">X</div>
        </a>
    </li>
  `);
  
  let $page = jQuery(`
    <div class = "page selected" id = page${tab_index}>   </div>  
  `);
  
  jQuery(".tab").removeClass("active");
  jQuery(this).parent().before($tab);
  //jQuery(".content").append($page);
  
  jQuery(".tab").click(function() {
    tab_index = jQuery(this).index() + 1;
    jQuery(".tab, .page").removeClass("selected");
    jQuery(".header .tab:nth-child(" + tab_index + ")").addClass("selected");
    jQuery(".content .page:nth-child(" + tab_index + ")").addClass("selected");
  });
  
  jQuery(".close-tab-btn").click(function() {
    //tab_index = jQuery(this).parent().parent().index() + 1;
    jQuery(this).parent().parent().remove();
    //jQuery(".tab-nav-header .tab:nth-child(" + tab_index + ")").remove();
    //jQuery(".content .page:nth-child(" + tab_index + ")").remove();   
  });
});*/




