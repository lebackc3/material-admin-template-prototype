jQuery( document ).ready(function() {
    fetch('https://api.airtable.com/v0/appOjRhDeMPwWxELo/Table%201?view=Grid%20view', {
    headers: new Headers({
        "Authorization" : "Bearer keyFLaWelkvPqvnDU"
    })
    }).then(function(response) {
        return response.json();
    })
    .then(function(myJson) {
        myJson.records.forEach(element => {
            let el_header = element.fields.Element_Header;
            let el_id = element.fields.Element_ID;
            jQuery("#" + el_id + " h2").text(el_header);
        });
    });
});
